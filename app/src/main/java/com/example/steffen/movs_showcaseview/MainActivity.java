package com.example.steffen.movs_showcaseview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private ShowcaseView showcaseView;
    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        showcaseView = new ShowcaseView.Builder(this)
                .withNewStyleShowcase()
                .setTarget(new ViewTarget(R.id.tv_hello, this))
                .setContentTitle("ShowcaseView")
                .setContentText("This is highlighting the Home button")
                .hideOnTouchOutside()
                .setStyle(R.style.CustomShowcaseTheme3)
                .setOnClickListener(this)
                //.singleShot(1)
                .build();


    }

    @Override
    public void onClick(View v) {
        switch (counter) {
            case 0:
                //showcaseView.setShowcase(new ViewTarget(findViewById(R.id.tv_hello)), true);
                showcaseView.setTarget(new ViewTarget(R.id.button, this));
                showcaseView.setStyle(R.style.CustomShowcaseThemeMOVS);

                break;

            case 1:
                //showcaseView.setTarget(new ViewTarget(R.id.checkBox, this));
                showcaseView.setShowcase(new ViewTarget(findViewById(R.id.checkBox)), true);
                break;


            case 2:
                showcaseView.setTarget(new ViewTarget(R.id.button, this));
                //showcaseView.setShowcase(new ViewTarget(findViewById(R.id.button)), false);
                break;

            case 3:
                showcaseView.setTarget(Target.NONE);
                showcaseView.setContentTitle("Check it out");
                showcaseView.setContentText("You don't always need a target to showcase");
                showcaseView.setButtonText("Close");
                break;

            case 4:
                showcaseView.hide();
        }
        counter++;
    }

}

